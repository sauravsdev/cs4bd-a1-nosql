# CS4BD-A1-NoSQL
### Repository for the assignment A1-NoSQL  

**Course Name**: Computer Science for Big Data  
**Program**: Masters in Data Science  
**Semester**: Winter 2019-2020  
**Course Teacher**: Prof. Stefan Edlich  
**Contributor**: Saurav Kumar Saha  
#
#
#### Solutions:
**#1** https://bitbucket.org/sauravsdev/cs4bd-a1-nosql/src/94c8a836e864/1/redis-benchmark.txt?at=master  
**#2** https://bitbucket.org/sauravsdev/cs4bd-a1-nosql/src/94c8a836e864/2/crud-with-redis-data-structures.txt?at=master  
**#3** https://bitbucket.org/sauravsdev/cs4bd-a1-nosql/src/94c8a836e864/3/crud-with-mongo-collection.txt?at=master  
**#4** https://bitbucket.org/sauravsdev/cs4bd-a1-nosql/src/94c8a836e864/redisapp/src/main/java/redisapp/App.java?at=master  
**#5** https://bitbucket.org/sauravsdev/cs4bd-a1-nosql/src/94c8a836e864/mongoapp/src/main/java/mongoapp/App.java?at=master  

#### Task Description
- (1a) Install a Key/Value DB as Redis (requires either UNIX make or the Docker image) and run the redis-benchmark!. [file #1]
#
- (1b) Play with the Shell. Do some CRUDs for the basic datastructures (do not use messages and streaming yet).
  The basic datatructures are: Key/Value, Hashes, Lists, Sets / Sorted Sets.
  Save it like a text protocol or even screenshots in the worst case. [file #2]
#
- (2) Do the same as above (doing CRUD in a Shell) for either a GraphDB (e.g. neo4j) or a DocumentDB (e.g. Elastic (!) or MongoDB).
  Or do everything in a Multimodel DB (as ArangoDB which has a nice Docker image too). [file #3]
#
- (3) Code CRUD Ops against the Key-Value DB and one of the DBs mentioned in Nr.2 in any language you like (Python?!).
  (If using Redis only against one data structure is ok). [file #4, file #5]

Show the solution best till 16th of November!
#
#
#
#### Start/Stop redis docker container and using with shell
* Install Docker Daemon (Docker Desktop for Windows OS)
* Go to "<project-dir>/docker-compose/redis_cs4bd" (use PowerShell/CommandPrompt/Terminal)
* Start redis container in detached mode (as service)
	- docker-compose up -d
* Enter into redis container's shell with interactive mode
	- docker exec -it redis_cs4bd bash
* To benchmark use (After entering into shell)
	- redis-benchmark
* To connect with redis server
	- redis-cli -h 127.0.0.1 -p 6379
* To stop redis container (After exiting redis-cli and container-shell with 'exit')
	- docker-compose down
#
#
#### Start/Stop mongodb docker container and using with shell
* Install Docker Daemon (Docker Desktop for Windows OS)
* Go to "<project-dir>/docker-compose/mongodb_cs4bd" (use PowerShell/CommandPrompt/Terminal)
* Start mongodb container in detached mode (as service)
	- docker-compose up -d
* Enter into mongodb container's shell with interactive mode
	- docker exec -it mongodb_cs4bd bash
* To connect with mongodb server / enter into mongodb-shell
	- mongo -u cs4bd -p cs4bd --authenticationDatabase cs4bd
* To stop mongodb container (After exiting mongodb-shell and container-shell with 'exit')
	- docker-compose down

#
#
#### Reference Links:
 - https://github.com/redisson/redisson
 - https://mongodb.github.io/mongo-java-driver/
 - https://mongodb.github.io/mongo-java-driver/3.5/driver/getting-started/quick-start-pojo/
