package redisapp;

import java.util.Comparator;

/**
 * Created by IntelliJ IDEA.
 *
 * @author saurav
 */
public class StudentComparator implements Comparator<Student> {
    @Override
    public int compare(Student s1, Student s2) {
        return s1.getStudentId().compareTo(s2.getStudentId());
    }
}
