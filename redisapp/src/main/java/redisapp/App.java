package redisapp;

import com.google.common.collect.Lists;
import org.redisson.Redisson;
import org.redisson.api.RSortedSet;
import org.redisson.api.RedissonClient;
import org.redisson.codec.JsonJacksonCodec;
import org.redisson.config.Config;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 *
 * @author saurav
 */
public class App {

    public static void main(String[] args) {

        // Configuration for RedissonClient
        Config config = new Config();
        config.useSingleServer().setAddress("redis://localhost:9736");
        config.setCodec(new JsonJacksonCodec());

        // Initializing RedissonClient
        RedissonClient client = null;
        try {
            client = Redisson.create(config);

            // Using RSortedSet, which uses List data structure of Redis underneath

            // Create/Persist a list of students
            System.out.println();
            System.out.println("Create/Persist OP: [Started]");
            RSortedSet<Student> redisStudents = client.getSortedSet("students-ds");
            if (redisStudents.isEmpty()) {
                redisStudents.trySetComparator(new StudentComparator());
            }
            students().forEach(student -> {
                redisStudents.remove(student);
                redisStudents.add(student);
            });
            printAll(redisStudents);
            System.out.println("Create/Persist OP: [Completed]");


            // Reading all students information
            System.out.println();
            System.out.println("Read/Retrieve OP: [Started]");
            printAll(redisStudents);
            System.out.println("Read/Retrieve OP: [Completed]");


            // Updating an entry of students
            System.out.println();
            System.out.println("Update OP: [Started]");
            Student toBeUpdated = s81481();
            String anotherEmail = "sks.sauravs@yahoo.com";
            toBeUpdated.getEmails().add(anotherEmail);
            if (redisStudents.contains(toBeUpdated)) {
                redisStudents.remove(toBeUpdated);
                redisStudents.add(toBeUpdated);
            }
            printAll(redisStudents);
            System.out.println("Update OP: [Completed]");

            // Deleting an entry from students
            System.out.println();
            System.out.println("Delete OP: [Started]");
            Student toBeDeleted = s81473();
            redisStudents.remove(toBeDeleted);
            printAll(redisStudents);
            System.out.println("Delete OP: [Completed]");

        } catch (Exception exception) {
            exception.printStackTrace();
        } finally {
            if (client != null) {
                client.shutdown();
            }
        }
    }

    private static List<Student> students() {
        return Arrays.asList(
            s81481(),
            s81482(),
            s81473()
        );
    }

    private static Student s81481() {
        return Student.builder()
            .studentId("s81481")
            .firstName("Saurav Kumar")
            .lastName("Saha")
            .emails(
                Lists.newArrayList(
                    "s81481@beuth-hochschule.de"

                )
            )
            .country("Bangladesh")
            .build();
    }

    private static Student s81482() {
        return Student.builder()
            .studentId("s81482")
            .firstName("Rimsha")
            .lastName("Saif")
            .emails(
                Lists.newArrayList(
                    "s81482@beuth-hochschule.de"
                )
            )
            .country("Pakistan")
            .build();
    }

    private static Student s81473() {
        return Student.builder()
            .studentId("s81473")
            .firstName("Akshat")
            .lastName("Gupta")
            .emails(
                Lists.newArrayList(
                    "s81473@beuth-hochschule.de"
                )
            )
            .country("India")
            .build();
    }

    private static void printAll(RSortedSet<Student> redisStudents) {
        Collection<Student> allStudents = redisStudents.readAll();
        allStudents.forEach(System.out::println);
    }
}
