package redisapp;

import lombok.*;

import java.io.Serializable;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 *
 * @author saurav
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class Student implements Serializable, Comparable<Student> {
    private static final long serialVersionUID = 771652260758459933L;
    private String studentId;
    private String firstName;
    private String lastName;
    private List<String> emails;
    private String country;

    @Override
    public int compareTo(Student other) {
        return this.getStudentId().compareTo(other.getStudentId());
    }
}
