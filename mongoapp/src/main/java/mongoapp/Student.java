package mongoapp;

import lombok.*;
import org.bson.types.ObjectId;

import java.io.Serializable;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 *
 * @author saurav
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class Student implements Serializable {
    private static final long serialVersionUID = 771652260758459933L;
    private ObjectId id;
    private String studentId;
    private String firstName;
    private String lastName;
    private List<String> emails;
    private String country;
}
