package mongoapp;

import com.google.common.collect.Lists;
import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.IndexOptions;
import com.mongodb.client.model.Indexes;
import org.bson.BsonDocument;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;

import java.util.Arrays;
import java.util.List;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Sorts.ascending;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

/**
 * Created by IntelliJ IDEA.
 *
 * @author saurav
 */
public class App {
    public static void main(String[] args) {
        // Specifying connection URI, Username: cs4bd, Password: cs4bd, Host: localhost, Port: 37017, Database: cs4bd
        MongoClientURI uri = new MongoClientURI("mongodb://cs4bd:cs4bd@localhost:37017/cs4bd");

        // Initializing MongoClient
        try (MongoClient client = new MongoClient(uri)) {

            // Switching to working Database
            MongoDatabase cs4bd = client.getDatabase("cs4bd");

            // Initializing Java POJO codec registry for "students-ds" collection
            CodecRegistry pojoCodecRegistry = fromRegistries(
                MongoClient.getDefaultCodecRegistry(),
                fromProviders(
                    PojoCodecProvider.builder().automatic(true).build()
                )
            );
            MongoCollection<Student> mongoStudents = cs4bd
                .getCollection("students-ds", Student.class)
                .withCodecRegistry(pojoCodecRegistry);

            // Creating index based on "studentId" property
            IndexOptions indexOptions = new IndexOptions().unique(true);
            mongoStudents.createIndex(Indexes.ascending("studentId"), indexOptions);

            // for printing blocks
            Block<Student> printBlock = new Block<Student>() {
                @Override
                public void apply(final Student student) {
                    System.out.println(student);
                }
            };


            // Create/Persist a list of students
            System.out.println();
            System.out.println("Create/Persist OP: [Started]");
            mongoStudents.deleteMany(new BsonDocument());
            mongoStudents.insertMany(students());
            System.out.println("Create/Persist OP: [Completed]");


            // Reading all students information
            System.out.println();
            System.out.println("Read/Retrieve OP: [Started]");
            mongoStudents.find().sort(ascending("studentId")).forEach(printBlock);
            System.out.println("Read/Retrieve OP: [Completed]");


            // Updating an entry of students
            System.out.println();
            System.out.println("Update OP: [Started]");
            Student toBeUpdated = s81481();
            String anotherEmail = "sks.sauravs@yahoo.com";
            toBeUpdated.getEmails().add(anotherEmail);
            mongoStudents.replaceOne(eq("studentId", toBeUpdated.getStudentId()), toBeUpdated);
            mongoStudents.find(eq("studentId", toBeUpdated.getStudentId())).forEach(printBlock);
            System.out.println("Update OP: [Completed]");


            // Deleting an entry from students
            System.out.println();
            System.out.println("Delete OP: [Started]");
            Student toBeDeleted = s81473();
            mongoStudents.deleteOne(eq("studentId", toBeDeleted.getStudentId()));
            mongoStudents.find().sort(ascending("studentId")).forEach(printBlock);
            System.out.println("Delete OP: [Completed]");

        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    private static List<Student> students() {
        return Arrays.asList(
            s81481(),
            s81482(),
            s81473()
        );
    }

    private static Student s81481() {
        return Student.builder()
            .studentId("s81481")
            .firstName("Saurav Kumar")
            .lastName("Saha")
            .emails(
                Lists.newArrayList(
                    "s81481@beuth-hochschule.de"

                )
            )
            .country("Bangladesh")
            .build();
    }

    private static Student s81482() {
        return Student.builder()
            .studentId("s81482")
            .firstName("Rimsha")
            .lastName("Saif")
            .emails(
                Lists.newArrayList(
                    "s81482@beuth-hochschule.de"
                )
            )
            .country("Pakistan")
            .build();
    }

    private static Student s81473() {
        return Student.builder()
            .studentId("s81473")
            .firstName("Akshat")
            .lastName("Gupta")
            .emails(
                Lists.newArrayList(
                    "s81473@beuth-hochschule.de"
                )
            )
            .country("India")
            .build();
    }
}
